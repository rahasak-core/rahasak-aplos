akka {
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = DEBUG

  kafka {
    consumer {
      # Tuning property of scheduled polls.
      poll-interval = 50ms

      # Tuning property of the `KafkaConsumer.poll` parameter.
      # Note that non-zero value means that blocking of the thread that
      # is executing the stage will be blocked.
      poll-timeout = 50ms

      # The stage will be await outstanding offset commit requests before
      # shutting down, but if that takes longer than this timeout it will
      # stop forcefully.
      stop-timeout = 30s

      # How long to wait for `KafkaConsumer.close`
      close-timeout = 20s

      # If offset commit requests are not completed within this timeout
      # the returned Future is completed `TimeoutException`.
      commit-timeout = 15s

      # If the KafkaConsumer can't connect to the broker the poll will be
      # aborted after this timeout. The KafkaConsumerActor will throw
      # org.apache.kafka.common.errors.WakeupException, which can be handled
      # with Actor supervision strategy.
      wakeup-timeout = 10s

      # Fully qualified config path which holds the dispatcher configuration
      # to be used by the KafkaConsumerActor. Some blocking may occur.
      use-dispatcher = "akka.kafka.default-dispatcher"

      # Properties defined by org.apache.kafka.clients.consumer.ConsumerConfig
      # can be defined in this configuration section.
      kafka-clients {
        # auto-commit disabled by default
        # Setting enable.auto.commit means that offsets are committed automatically
        #  with a frequency controlled by the config auto.commit.interval.ms.
        enable.auto.commit = true

        auto.offset.reset = "earliest"

        auto.commit.interval.ms = 10000
      }
    }
    producer {
      # Tuning parameter of how many sends that can run in parallel.
      parallelism = 100

      # How long to wait for `KafkaProducer.close`
      close-timeout = 60s

      # Fully qualified config path which holds the dispatcher configuration
      # to be used by the producer stages. Some blocking may occur.
      # When this value is empty, the dispatcher configured for the stream
      # will be used.
      use-dispatcher = "akka.kafka.default-dispatcher"

      # The time interval to commit a transaction when using the `Transactional.sink` or `Transactional.flow`
      eos-commit-interval = 100ms
    }
  }
}

service {
  mode = "DEV"
  mode = ${?SERVICE_MODE}
  name = "aplos"
  name = ${?SERVICE_NAME}
}

keys {
  dir = ".keys"
  public-key-location = ".keys/id_rsa.pub"
  private-key-location = ".keys/id_rsa"
}


