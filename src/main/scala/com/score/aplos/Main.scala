package com.score.aplos

import akka.actor.ActorSystem
import com.score.aplos.actor.AplosActor
import com.score.aplos.actor.AplosActor.Stream
import com.score.aplos.storage.AplosStore
import com.score.aplos.config.AppConf
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.util.{LoggerFactory, RSAFactory}

object Main extends App with AppConf {

  // actor system mystiko
  implicit val system = ActorSystem.create("rahasak")

  // setup logging
  LoggerFactory.init()

  // set up keys
  RSAFactory.init()

  // create schema/indexes
  AplosStore.init()
  ElasticStore.init()

  // init aplos
  system.actorOf(AplosActor.props(), name = "AplosActor") ! Stream

}
