package com.score.aplos.storage

import java.util.Date

import com.dataoperandz.cassper.Cassper
import com.score.aplos.util.AppLogger

case class Account(address: String, balance: Int, pubKey: String, timestamp: Date = new Date)

case class Trans(id: String, execer: String, actor: String, messageTyp: String, message: String, digsig: String, timestamp: Date = new Date)

object AplosStore extends AplosCluster with AppLogger {

  lazy val caps = session.prepare("INSERT INTO mystiko.accounts(address, balance, pub_key, timestamp) VALUES(?, ?, ?, ?)")
  lazy val gaps = session.prepare("SELECT * FROM mystiko.accounts where address = ? LIMIT 1")
  lazy val uaps = session.prepare("UPDATE mystiko.accounts SET balance = ? WHERE address = ?")
  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ? LIMIT 1")
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message_typ, message, digsig, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")

  def init() = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    Cassper.build(cassandraKeyspace, session)
      .migrate(cassandraKeyspace)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, actor, id)).one()
    row != null
  }

  def createTrans(trans: Trans) = {
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.messageTyp, trans.message,
      trans.digsig, trans.timestamp))
  }

  def createAccount(account: Account) = {
    // create account
    println(System.currentTimeMillis())
    session.execute(caps.bind(account.address, account.balance: java.lang.Integer, account.pubKey, account.timestamp))
    println(System.currentTimeMillis())
  }

  def getAccount(address: String): Option[Account] = {
    // account object
    val row = session.execute(gaps.bind(address)).one()
    if (row != null) Option(Account(row.getString("address"), row.getInt("balance"),
      row.getString("pub_key"), row.getTimestamp("timestamp")))
    else None
  }

  def updateBalance(address: String, balance: Int) = {
    // update balance
    session.execute(uaps.bind(balance: java.lang.Integer, address))
  }

}

//object M extends App {
//  CassandraStore.init()
//  //println(CassandraStore.isDoubleSpend("eranga", "112"))
//  CassandraStore.createAccount(Account("2111", 2000, ""))
//  //println(CassandraStore.getAccount("1111"))
//  //CassandraStore.updateBalance("1111", 7880)
//  //CassandraStore.createTrans(Trans("112", "eranga", "AccountActor", "{\"message\": \"java\"}"))
//}