package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.score.aplos.actor.AccountActor.{Create, Get, Put, Transfer}
import com.score.aplos.actor.AplosActor.Stream
import com.score.aplos.config.{FeatureToggleConf, KafkaConf}
import com.score.aplos.protocol.AccountMessage
import com.score.aplos.util.{AppLogger, RSAFactory}
import org.apache.kafka.common.serialization.StringDeserializer
import spray.json._

object AplosActor {

  case class Stream()

  def props() = Props(new AplosActor)

}

class AplosActor() extends Actor with AppLogger with KafkaConf with FeatureToggleConf {

  override def receive: Receive = {
    case Stream =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(context.system).withSupervisionStrategy(decider))
      implicit val ec = context.system.dispatcher

      // kafka source
      val consumerSettings = ConsumerSettings(context.system, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaAddr)
        .withGroupId(kafkaGroup)
      val source = Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaTopic))

      // consumer as stream
      source
        .map(kmsg => {
          // got the message
          val msg = kmsg.record.value
          logger.info(s"got actor message - $msg")

          msg match {
            case _ if msg.contains("AccountActor") =>
              // for account actor
              logger.info(s"msg for account actor - $msg")

              import com.score.aplos.protocol.AccountMessageProtocol._
              val accMsg = msg.parseJson.convertTo[AccountMessage]

              // verify digital signature if feature toggle enabled
              if (enableVerifySignature) {
                RSAFactory.verifySignature(accMsg.toString)
              }

              accMsg match {
                case create: Create =>
                  context.actorOf(AccountActor.props()) ! create
                case put: Put =>
                  context.actorOf(AccountActor.props()) ! put
                case get: Get =>
                  context.actorOf(AccountActor.props()) ! get
                case transfer: Transfer =>
                  context.actorOf(AccountActor.props()) ! transfer
              }
            case _ =>
              // for other actor
              logger.info(s"msg for unknown actor - $msg")
          }
        })
        .runWith(Sink.ignore)
  }

}
