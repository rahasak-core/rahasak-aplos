package com.score.aplos.actor

import akka.actor.{Actor, Props}
import com.score.aplos.actor.AccountActor._
import com.score.aplos.storage.{Account, AplosStore, Trans}
import com.score.aplos.protocol.AccountMessage
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.AppLogger
import spray.json.DefaultJsonProtocol._
import spray.json._

object AccountActor {

  case class Create(actor: String, messageType: String, execer: String, id: String, address: String, balance: Int, pubKey: String, digsig: String) extends AccountMessage

  case class Put(actor: String, messageType: String, execer: String, id: String, address: String, field: String, value: String, digsig: String) extends AccountMessage

  case class Get(actor: String, messageType: String, execer: String, id: String, address: String, digsig: String) extends AccountMessage

  case class Transfer(actor: String, messageType: String, execer: String, id: String, fAddress: String, tAddress: String, amount: Int, digsig: String) extends AccountMessage

  def props() = Props(new AccountActor)

}

class AccountActor extends Actor with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};AccountActor;${create.id}"
      if (!AplosStore.isDoubleSpend(create.execer, "AccountActor", create.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Create] = jsonFormat8(Create)
        val trans = Trans(create.id, create.execer, "AccountActor", "Create", create.toJson.toString, create.digsig)
        AplosStore.createTrans(trans)

        // create account on aplos store
        val acc = Account(create.address, create.balance, create.pubKey)
        AplosStore.createAccount(acc)

        logger.info(s"init done $create")
      } else {
        logger.error(s"double spend init - $create")
      }
    case put: Put =>
      logger.info(s"got put message - $put")

      // first check double spend
      val id = s"${put.execer};AccountActor;${put.id}"
      if (!AplosStore.isDoubleSpend(put.execer, "AccountActor", put.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Put] = jsonFormat8(Put)
        val trans = Trans(put.id, put.execer, "AccountActor", "Put", put.toJson.toString, put.digsig)
        AplosStore.createTrans(trans)

        // update account on aplos store

        logger.info(s"put done - $put")
      } else {
        logger.error(s"double spend put $put")
      }
    case get: Get =>
      logger.info(s"got get message - $get")

      // get account from aplos store
      // no need to create transaction here since not updating the ledger
      AplosStore.getAccount(get.address)
    case transfer: Transfer =>
      logger.info(s"got transfer message - $transfer")

      // first check double spend
      val id = s"${transfer.execer};AccountActor;${transfer.id}"
      if (!AplosStore.isDoubleSpend(transfer.execer, "AccountActor", transfer.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Transfer] = jsonFormat8(Transfer)
        val trans = Trans(transfer.id, transfer.execer, "AccountActor", "Transfer",transfer.toJson.toString, transfer.digsig)
        AplosStore.createTrans(trans)

        // update balance
        val facc = AplosStore.getAccount(transfer.fAddress)
        val tacc = AplosStore.getAccount(transfer.tAddress)
        (facc, tacc) match {
          case (Some(f), Some(t)) =>
            AplosStore.updateBalance(f.address, f.balance - transfer.amount)
            AplosStore.updateBalance(t.address, t.balance + transfer.amount)

            logger.info(s"transfer done - $transfer")
          case _ =>
            logger.error(s"no matching account found for transfer - $transfer faccount - $facc, taccount - $tacc")
        }
      } else {
        // error since double spend
        logger.error(s"double spend transfer $transfer")
      }
  }
}
