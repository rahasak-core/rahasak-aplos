package com.score.aplos.protocol

import com.score.aplos.actor.AccountActor._
import spray.json._

trait AccountMessage

object AccountMessageProtocol extends DefaultJsonProtocol {

  implicit val initFormat: JsonFormat[Create] = jsonFormat8(Create)
  implicit val putFormat: JsonFormat[Put] = jsonFormat8(Put)
  implicit val getFormat: JsonFormat[Get] = jsonFormat6(Get)
  implicit val transferFormat: JsonFormat[Transfer] = jsonFormat8(Transfer)

  implicit object AccountMessageFormat extends RootJsonFormat[AccountMessage] {
    def write(obj: AccountMessage): JsValue =
      JsObject((obj match {
        case i: Create => i.toJson
        case p: Put => p.toJson
        case g: Get => g.toJson
        case t: Transfer => t.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): AccountMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("put")) => json.convertTo[Put]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("transfer")) => json.convertTo[Transfer]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
//
//object M extends App {
//
//  import AccountMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", 100, "pubkey")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[AccountMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}

